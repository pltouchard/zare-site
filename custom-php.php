<?php
/* Template Name: Form Process */
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Reservation {
	private $dateResa;
	private $nomResa;
	private $telephoneResa;
	private $mailResa;
	private $nbCouvertsResa;
	private $creneauResa;
	private $serviceResa;
	private $showResa;
	private $notesResa;
	private $notesResaImportant;
	private $habitue;

	public function __construct($dateResa, $nomResa, $telephoneResa, $mailResa, $nbCouvertsResa, $creneauResa, $serviceResa, $showResa, $notesResa, $notesResaImportant, $habitue)
	{
		$this->dateResa = $dateResa;
		$this->nomResa = $nomResa;
		$this->telephoneResa = $telephoneResa;
		$this->mailResa = $mailResa;
		$this->nbCouvertsResa = $nbCouvertsResa;
		$this->creneauResa = $creneauResa;
		$this->serviceResa = $serviceResa;
		$this->showResa = $showResa;
		$this->notesResa = $notesResa;
		$this->notesResaImportant = $notesResaImportant;
		$this->habitue = $habitue;
	}

		/**
	 * Get the value of date
	 */ 
	public function getDateResa()
	{
		return $this->dateResa;
	}

	/**
	 * Set the value of date
	 *
	 * @return  self
	 */ 
	public function setDateResa($dateResa)
	{
		$this->dateResa = $dateResa;

		return $this;
	}

	/**
	 * Get the value of nomResa
	 */ 
	public function getNomResa()
	{
		return $this->nomResa;
	}

	/**
	 * Set the value of nomResa
	 *
	 * @return  self
	 */ 
	public function setNomResa($nomResa)
	{
		$this->nomResa = $nomResa;

		return $this;
	}

	/**
	 * Get the value of telephoneResa
	 */ 
	public function getTelephoneResa()
	{
		return $this->telephoneResa;
	}

	/**
	 * Set the value of telephoneResa
	 *
	 * @return  self
	 */ 
	public function setTelephoneResa($telephoneResa)
	{
		$this->telephoneResa = $telephoneResa;

		return $this;
	}

	/**
	 * Get the value of mailResa
	 */ 
	public function getMailResa()
	{
		return $this->mailResa;
	}

	/**
	 * Set the value of mailResa
	 *
	 * @return  self
	 */ 
	public function setMailResa($mailResa)
	{
		$this->mailResa = $mailResa;

		return $this;
	}

	/**
	 * Get the value of nbCouvertsResa
	 */ 
	public function getNbCouvertsResa()
	{
		return $this->nbCouvertsResa;
	}

	/**
	 * Set the value of nbCouvertsResa
	 *
	 * @return  self
	 */ 
	public function setNbCouvertsResa($nbCouvertsResa)
	{
		$this->nbCouvertsResa = $nbCouvertsResa;

		return $this;
	}

	/**
	 * Get the value of creneauResa
	 */ 
	public function getCreneauResa()
	{
		return $this->creneauResa;
	}

	/**
	 * Set the value of creneauResa
	 *
	 * @return  self
	 */ 
	public function setCreneauResa($creneauResa)
	{
		$this->creneauResa = $creneauResa;

		return $this;
	}

	/**
	 * Get the value of serviceResa
	 */ 
	public function getServiceResa()
	{
		return $this->serviceResa;
	}

	/**
	 * Set the value of serviceResa
	 *
	 * @return  self
	 */ 
	public function setServiceResa($serviceResa)
	{
		$this->serviceResa = $serviceResa;

		return $this;
	}

	/**
	 * Get the value of showResa
	 */ 
	public function getShowResa()
	{
		return $this->showResa;
	}

	/**
	 * Set the value of showResa
	 *
	 * @return  self
	 */ 
	public function setShowResa($showResa)
	{
		$this->showResa = $showResa;

		return $this;
	}

	/**
	 * Get the value of notesResa
	 */ 
	public function getNotesResa()
	{
		return $this->notesResa;
	}

	/**
	 * Set the value of notesResa
	 *
	 * @return  self
	 */ 
	public function setNotesResa($notesResa)
	{
		$this->notesResa = $notesResa;

		return $this;
	}


	/**
	 * Get the value of notesResaImportant
	 */ 
	public function getNotesResaImportant()
	{
		return $this->notesResaImportant;
	}

	/**
	 * Set the value of notesResaImportant
	 *
	 * @return  self
	 */ 
	public function setNotesResaImportant($notesResaImportant)
	{
		$this->notesResaImportant = $notesResaImportant;

		return $this;
	}

	/**
	 * Get the value of habitue
	 */ 
	public function getHabitue()
	{
		return $this->habitue;
	}

	/**
	 * Set the value of habitue
	 *
	 * @return  self
	 */ 
	public function setHabitue($habitue)
	{
		$this->habitue = $habitue;

		return $this;
	}

	public function expose() {
		return get_object_vars($this);
	}
	
}

class Creneau {
	private $idCreneau;
	private $creneauDebut;
	private $creneauFin;
	
	public function __construct($idCreneau, $creneauDebut, $creneauFin)
	{
		$this->idCreneau = $idCreneau;
		$this->creneauDebut = $creneauDebut;
		$this->creneauFin = $creneauFin;
	}


	/**
	 * Get the value of idCreneau
	 */ 
	public function getIdCreneau()
	{
		return $this->idCreneau;
	}

	/**
	 * Set the value of idCreneau
	 *
	 * @return  self
	 */ 
	public function setIdCreneau($idCreneau)
	{
		$this->idCreneau = $idCreneau;

		return $this;
	}

	/**
	 * Get the value of creneauDebut
	 */ 
	public function getCreneauDebut()
	{
		return $this->creneauDebut;
	}

	/**
	 * Set the value of creneauDebut
	 *
	 * @return  self
	 */ 
	public function setCreneauDebut($creneauDebut)
	{
		$this->creneauDebut = $creneauDebut;

		return $this;
	}

	/**
	 * Get the value of creneauFin
	 */ 
	public function getCreneauFin()
	{
		return $this->creneauFin;
	}

	/**
	 * Set the value of creneauFin
	 *
	 * @return  self
	 */ 
	public function setCreneauFin($creneauFin)
	{
		$this->creneauFin = $creneauFin;

		return $this;
	}
	
	public function expose() {
		return get_object_vars($this);
	}

	
}

if ( isset( $_POST['submit'] ) ) {

	if(isset($_POST['dateResa']) && !empty($_POST['dateResa']) 
	&& isset($_POST['nomResa']) && !empty($_POST['nomResa']) 
	&& isset($_POST['telephoneResa']) && !empty($_POST['telephoneResa'])  
	&& isset($_POST['mailResa']) && !empty($_POST['mailResa'])  
	&& isset($_POST['nbCouvertsResa']) && !empty($_POST['nbCouvertsResa'])  
	&& isset($_POST['creneauResaId']) && !empty($_POST['creneauResaId'])){
		$date_resa = filter_var ( $_POST['dateResa'], FILTER_SANITIZE_STRING);
		$nom_resa = filter_var ( $_POST['nomResa'], FILTER_SANITIZE_STRING);
		$telephone_resa = filter_var ( $_POST['telephoneResa'], FILTER_SANITIZE_NUMBER_INT);
		$mail_resa = filter_var ( $_POST['mailResa'], FILTER_SANITIZE_EMAIL);
		$nb_couverts_resa = filter_var ( $_POST['nbCouvertsResa'], FILTER_SANITIZE_NUMBER_INT);
		$creneau_resa_id = filter_var ( $_POST['creneauResaId'], FILTER_SANITIZE_NUMBER_INT);
		$notes_resa = filter_var ( $_POST['notesResa'], FILTER_SANITIZE_STRING);
		
	}else{
		wp_redirect( "http://zare.preprod-hoppopup.com?message=echec#reservation" );
		exit;
	}
	
	if(isset($_POST['notesResa'])){
		$notes_resa = $_POST['notesResa'];
	}

	//retrieve creneau
	$url_get_creneau = "http://hoppopup.ddns.net:8080/zare-api/api/creneau/" . $creneau_resa_id;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
	curl_setopt($ch, CURLOPT_URL, $url_get_creneau);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$creneau_retreived = curl_exec($ch);
	curl_close($ch);

	
	$decoded_creneau = json_decode($creneau_retreived);

	//convert string result to Creneau formats
	$creneau_id = (int)$decoded_creneau->{"idCreneau"};

	//$creneau_debut = date("H:i:s", strtotime($decoded_creneau->{"creneauDebut"}));
	$obj_date_1 = DateTime::createFromFormat('H:i:s', $decoded_creneau->{"creneauDebut"});
	$creneau_debut = $obj_date_1->format("H:i:s");

	//$creneau_fin = date("H:i:s", strtotime($decoded_creneau->{"creneauFin"}));
	$obj_date_2 = DateTime::createFromFormat('H:i:s', $decoded_creneau->{"creneauFin"});
	$creneau_fin = $obj_date_2->format("H:i:s");

	$creneau_resa = new Creneau($creneau_id, $creneau_debut, $creneau_fin);

	//set service resa
	$limit_creneau = DateTime::createFromFormat('H:i:s', "16:30:00");
	if($obj_date_1 < $limit_creneau){
		$service_resa_setted = "midi";
	}else{
		$service_resa_setted = "soir";
	}
	
	//create reservation
	$reservation = new Reservation($date_resa, $nom_resa, $telephone_resa, $mail_resa, $nb_couverts_resa, $creneau_resa->expose(), $service_resa_setted, true, $notes_resa, false, false);

	//post method
	$url_post_reservation = "http://hoppopup.ddns.net:8080/zare-api/api/reservation-ajout/";

	$re = curl_init();	
	curl_setopt($re, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
	curl_setopt($re, CURLOPT_URL, $url_post_reservation);
	curl_setopt($re, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($re, CURLOPT_POST,1 );
	curl_setopt($re, CURLOPT_POSTFIELDS, json_encode($reservation->expose())); 
	$post_result = curl_exec($re);
	$infos_request = curl_getinfo($re, CURLINFO_HTTP_CODE);
	curl_close($re);

	if($infos_request === 400)
	{
	 	wp_redirect( "http://zare.preprod-hoppopup.com?message=complet#reservation" );
		exit;
	}else if ($infos_request === 201){
		wp_redirect( "http://zare.preprod-hoppopup.com?message=reussite#reservation" );
		exit;
	}else{
		wp_redirect( "http://zare.preprod-hoppopup.com?message=echec#reservation" );
		exit;
	}

}