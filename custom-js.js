$( document ).ready(function() {
	var urlToContact = "http://hoppopup.ddns.net:8080/zare-api"
	//retrieve opening days to disable if not open
	let joursOuverts = [];
	getJoursOuverture();
	
	//function to check general opening days
	function getJoursOuverture(){

		getJoursOuverts=$.ajax({
			url: urlToContact + "/api/parametres-jours-ouverts",
			headers: {
				'Authorization': 'Bearer ' + aptok,
			},
			data: {
				method: 'GET', 
				dataType: 'json', 
			},
			success: function( result ) {

			}
		});

		getJoursOuverts.done(function(result){
			joursOuverts = result;
			getJoursOuvertureParticuliers();
		});
	}
	
	//function to check exceptionnal opening days
	function getJoursOuvertureParticuliers(){

		getJoursOuvertsParticulier=$.ajax({
			url: urlToContact + "/api/all-parametres-particuliers",
			headers: {
				'Authorization': 'Bearer ' + aptok,
			},
			data: {
				method: 'GET', 
				dataType: 'json', 
			},
			success: function( result ) {

			}
		});

		getJoursOuvertsParticulier.done(function(result){
			let today = Date.now();
			let listOfDays = [];
			if(result != undefined){
				result.forEach(el => {
					let dateToCompare = Date.parse(el.idParamP);
					if(dateToCompare >= today && !el.ouvertMidiP && !el.ouvertSoirP){
						listOfDays.push(el.idParamP);
					}

				});
			}
			$( "#datepicker" ).datepicker({
				dateFormat: "yy-mm-dd",
				numberOfMonths: 2,
				minDate:0,
				changeMonth: true,
				changeYear: true,
				beforeShowDay: function(date){
					let day = date.getDay();
					let string = jQuery.datepicker.formatDate('yy-mm-dd', date);
					if(joursOuverts[day] == false || listOfDays.indexOf(string) != -1){
						return [false]
					}
					return [true]
				}
			});
			//set day in form
			var initialDate = $( "#datepicker" ).datepicker( "getDate" );
			$("#dateAReserver").attr("value", formatDate(initialDate));
		});
	}
	
	
	
	//get créneaux on loadpage
	var auj = new Date();
	var aujFormat = formatDate(auj);
	getCreneauxByDateAjax(aujFormat);
	
	
	//change date in form by selected date in date picker
	$("#datepicker").on("change",function(){
		$("#creneauResaId")
			.empty()
			.append('<option value="0" selected="selected"> --Sélectionnez--</option>');;

		var selected = $(this).val();
		getCreneauxByDateAjax(selected);
		$("#dateAReserver").attr("value", selected);
	});
	
	//function to get créneaux by date and availability ajax
	function getCreneauxByDateAjax(date){
		//fill dropdown crénaux with available ones
		getCreneaux=$.ajax({
			url: urlToContact + "/api/creneaux-date?date=" + date,
			headers: {
				'Authorization': 'Bearer ' + aptok,
			},
			data: {
				method: 'GET', 
				dataType: 'json', 
			},
			success: function( result ) {

			}
		});

		//dropdown créneaux maker
		getCreneaux.done(function(result){
			let creneaux = document.getElementById('creneauResaId');
			if(result != undefined){
				result.forEach(element => {
					let tag = document.createElement("option");
					tag.setAttribute("value", element.idCreneau);
					tag.textContent = element.creneauDebut + ' - ' + element.creneauFin;
					creneaux.append(tag);
				});
			}
		});
	}
	
	//utils function to format date
	function formatDate(date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;

		return [year, month, day].join('-');
	}
	
	
	//process form submission
	var url_string = window.location.href;
	var url = new URL(url_string);
	var message = url.searchParams.get("message");
	
	if(message == "echec"){
		$('#message-form').html('<p class="error-msg">Une erreur est survenue, merci de vérifier le formulaire. En cas de problème multiples, ne pas hésiter à nous contacter par téléphone !</p>');
	}else if(message == "reussite"){
		$('#message-form').html('<p class="success-msg">Votre réservation a bien été effectuée. Merci de nous avertir en cas d\'annulation. Vous pouvez également nous contacter pour toute question.</p>');
	}else if(message == "complet"){
		$('#message-form').html('<p class="error-msg">Il semble que nous soyons complets pour la date/le service sélectionné ou qu\'il ne reste pas assez de place pour accueillir le nombre de personnes que vous indiquez. N\'hésitez pas à nous appeler pour voir si nous pouvons trouver une solution ! </p>');
	}else{
		$('#message-form').html("");
	}
	
});
